
#include "exampleplayer.h"
#include <stdlib.h> 
#include <stdio.h>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
     board = new Board();
     mySide = side;
     if (mySide == BLACK) oppSide = WHITE;
     else oppSide = BLACK;
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {

}

/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
    Move * temp = NULL;
    Board * boardCopy;
    board->doMove(opponentsMove, oppSide);
    int score;
    int maxscore = -200000;
    Move * move;
	if (!board->hasMoves(mySide)) return NULL;
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			move = new Move(i, j);
			if (board->checkMove(move, mySide)) {
				boardCopy = board->copy();
				boardCopy->doMove(move, mySide);
				score = boardCopy->countBlack() - boardCopy->countWhite();
				int mx = move->getX();
				int my = move->getY();
				//edges
				if (mx == 0 || mx == 7 || my == 0 || my == 7) score += 5;
				// corners
				if ((mx == 0 && my == 0) || (mx == 0 && my == 7) ||
					(mx == 7 && my == 0) || (mx == 7 && my == 7)) score += 3;
				// Adj. to corners
				if ((mx == 0 && my == 1) || (mx == 1 && my == 1) || 
					(mx == 1 && my == 0) || (mx == 0 && my == 6) ||
					(mx == 1 && my == 6) || (mx == 1 && my == 7) ||
					(mx == 6 && my == 0) || (mx == 6 && my == 1) ||
					(mx == 7 && my == 1) || (mx == 7 && my == 6) ||
					(mx == 6 && my == 6) || (mx == 6 && my == 7))
					score -= 8;
				// adj. to edges
				if (mx == 1 || mx == 6 || my == 1 || my == 6) score -= 5;
				//score -= moveDetract(boardCopy);
				if (score > maxscore) {
					maxscore = score;
					temp = new Move(move->getX(), move->getY());
				}
			}
		}
	}
	delete boardCopy;
	delete move;
	board->doMove(temp, mySide);
	return temp;
}
    
